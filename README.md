<h1>Package</h1>
<p>This package I have made to make it easier for me to make a certificate for webmail.</p>
<p>The following files are included
<ul>
    <li><b>nginxWebmailPartial.php</b>
        <ul>
            <li><b>Directory:</b> /usr/local/psa/admin/conf/templates/custom/webmail/nginxWebmailPartial.php</li>
            <li><b>Description:</b> Custom template for webmail redirection in Nginx</li>
        </ul>
    </li>
    <li><b>letsencrypt-webmail</b>
        <ul>
            <li><b>Directory:</b> /usr/bin/letsencrypt-webmail</li>
            <li><b>Description:</b> Used for making webmail certificates.</li>
        </ul>
    </li>
</ul>
<h1>Setup</h1>
<p>Connect with sftp to your server.</p>
<p>Copy ./usr/ directory in to the root of your server</p>

<p>Run the following command:</p>
```bash
/usr/local/psa/admin/sbin/httpdmng --reconfigure-server
```

<p>Your server is now reconfiguring the custom webmail nginx template.<br>
There is a minor change in the nginx template that makes it redirect immediatly to the HTTPS website without touching apache.</p>

<h2>Apache</h2>
<p>If you want to turn off HTTP to only serve HTTPS then you have to comment out the listen line in the httpd.conf which you can find in "/etc/httpd/conf".<br>
Turn off port 7080 for apache (port 80 from your browser)</p>

```bash
Search:
Listen 7080
Change to:
#Listen 7080
```
<br>
<h1>letsencrypt-webmail</h1>
<p>This little script is used to make certificates for the webmail applications in plesk.<br>I have made this because there was not really a solution for certificates in webmail.</p>
<h2>Usage</h2>
<p>only your preferred domain is required, <br>ex: ./letsencrypt-webmail example.com<br>If your dns records are right then letsencrypt-webmail will make a new certificate for you and it will automatically create the certificate in plesk<p>
<p>After you created a certificate with letsencrypt-webmail the last thing you have got to do is secure your webmail.<br>
<ol>
    <li>Go to Subscriptions > example.com > Websites & Domains > SSL/TLS Certificates for example.com</li>
    <li>Select the certificate named "webmail.example.com" and press Secure Webmail.</li>
</ol>
<p>Now your webmail is secured with the new certificate.</p>
<h2>It does not work!</h2>
<p>Dammit.. always the same story. Luckily some smartass in linux came up with making a file executable.<br>Try the following:</p>
```
chmod +x /usr/bin/letsencrypt-webmail
```
<p>It should work now!</p>
<h3>Still does not work..<h3>
<p>Have you installed the letsencrypt extension for plesk?</p>
<h1>Disclaimer</h1>
<p>This is for you guys, who always fuck things op somehow ;)</p>
<ul>
    <li>This is made for educational purposes</li>
    <li>You should only use this with plesk 17, because that is where this is based on.</li>
    <li>If your server explodes using this, I am certainly not responsible!</li>
</ul>

